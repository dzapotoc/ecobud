var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
	username: String,
	password: String, //hash actually
	email: String,
	created_at: {type: Date, default: Date.now}
});

var carSchema = new mongoose.Schema({
	_owner: {type: Number, ref: 'User'},
	make: String,
	model: String,
	year: Number,
	trips: [{
      distance: Number,
      fuel_used: Number,
      created_at: {type: Date, defualt: Date.now}
	}],
	created_at: {type: Date, default: Date.now}
});

mongoose.model("User", userSchema);
mongoose.model("Car", carSchema);