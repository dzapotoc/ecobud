var app = angular.module('myApp', ['ngRoute', 'ngResource']).run(function($rootScope) {
  $rootScope.car = '';

  $rootScope.isCarSelected = function() {
    return $rootScope.car.make != null;
  };
});

app.config(function($routeProvider) {

  $routeProvider   
    .when('/', {
      templateUrl: 'trip.html',
      controller: 'TripController'
    })
    .when('/addTrip', {
      templateUrl: 'add_trip.html',
      controller: 'AddTripController'
    });

});

app.factory('TripFactory', function($resource) {
  return $resource('/api/trips/:id');
});

app.factory('CarFactory', function($resource) {
  return $resource('/api/cars/:id');
});

app.controller('TripController', function($scope) {
  $scope.calcFuelEcon = function(trip) {
    return (trip.fuel_used / trip.distance) * 100;
  };

  $scope.getAverageFuelEcon = function(trips) {
    if (trips != null && trips.length > 0)
      return $scope.calcFuelEcon(trips.reduce(function(pre, cur) {
        pre.distance += cur.distance;
        pre.fuel_used += cur.fuel_used;
        return pre;
      }, {distance: 0, fuel_used: 0}));
  };
});

app.controller('CarSelectController', function($scope, $rootScope, CarFactory) {
  $scope.cars = CarFactory.query(function() {
    $scope.carSelect = $scope.cars[0]._id;
  });

  $scope.select = function(id) {
    $rootScope.car = CarFactory.get({id: id});
  };
});

app.controller('AddTripController', function($scope, $rootScope, TripFactory, $location) {
  $scope.trip = {distance: '', fuel_used: ''};

  $scope.addTrip = function() {
    $scope.trip.car = $rootScope.car._id;
    $rootScope.car = TripFactory.save($scope.trip);
    $location.path('/');
  };
});