var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var Car = mongoose.model('Car');

router.route('/trips')

  // .get(function(req, res) {
    
  //   Trip.find(function(err, trips) {
  //   	if (err)
  //   		return res.send(500, err);
  //   	return res.json(trips);
  //   });

  // })

  .post(function(req, res) {
    
    Car.findById(req.body.car, function(err, car) {
    	if (err)
    	  return res.send(500, err);

    	if (car == null)
    	  return res.json({message: "Car doesn't exist"});
    	
        var trip = {};
        trip.distance = req.body.distance;
        trip.fuel_used = req.body.fuel_used;
        trip.created_at = Date.now();
        car.trips.push(trip);

        car.save(function(err, car) {
          if (err)
            return res.send(500, err);
          return res.json(car);
        });
    });

  });

// router.route('/trips/:id')

//   .put(function(req,res){
    
//     Trip.findById(req.params.id, function(err, trip) {
//     	if (err)
//     		return res.send(500, err);

//     	trip.distance = req.body.distance;
//     	trip.fuel_used = req.body.fuel_used;
//     	trip.car = req.body.car;

//     	trip.save(function(err, trip) {
//     		if (err)
//     			return res.send(500, err);
//     		return res.json(trip);
//     	});
//     });

//   })

//   .get(function(req,res){
    
//     Trip.findById(req.params.id, function(err, trip) {
//     	if (err)
//     		return res.send(500, err);
//     	return res.json(trip);
//     });

//   })

//   .delete(function(req,res){
    
//     Trip.remove({_id: req.params.id}, function(err) {
//       if (err)
//       	return res.send(500, err);
//       return res.json("deleted :(");
//     });

//   });



router.route('/cars')

  .get(function(req, res) {
    
    Car.find({}, 'year make model',function(err, cars) {
    	if (err)
    	  return res.send(500, err);
    	return res.json(cars);
    });

  })

  .post(function(req, res) {
    
    var car = new Car();
    car.make = req.body.make;
    car.model = req.body.model;
    car.year = req.body.year;

    car.save(function(err, car) {
    	if (err)
    	  return res.send(500, err);
    	return res.json(car);
    });

  });

router.route('/cars/:id')

  .put(function(req,res){
    
    Car.findById(req.params.id, function(err, car) {
    	if (err)
    	  return res.send(500, err);

    	car.make = req.body.make;
    	car.model = req.body.model;
    	car.year = req.body.year;

    	car.save(function(err, car) {
    		if (err)
    		  return res.send(500, err);
    		return res.json(car);
    	});
    });

  })

  .get(function(req,res){
    
    Car.findById(req.params.id, function(err, car) {
    	if (err)
    	  return res.send(500, err);
    	return res.json(car);
    });

  })

  .delete(function(req,res){
    
    Car.remove({_id: req.params.id}, function(err) {
    	if (err)
    	  return res.send(500, err);
    	return res.json("deleted :(");
    });

  });

module.exports = router;
